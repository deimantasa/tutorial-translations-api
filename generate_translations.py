import json
import gspread
from oauth2client.service_account import ServiceAccountCredentials

FLUTTER_PROJECT_PATH = "/Users/mac/FlutterProject/tutorial_language_translator/assets/languages/"
CREDENTIALS_JSON_FILE_PATH = "credentials.json"
TRANSLATIONS_SHEET_NAME = 'Tutorial Translations'


def parse_language_to_json(languages_json, language, output_json_filename):
    language_json = {}
    for language_value in languages_json:
        page = language_value['Page']

        # Check if current page already exists
        page_already_exists = False
        if page in language_json:
            page_already_exists = True

        # If it doesn't exist - add new page as json object
        if not page_already_exists:
            language_json.update({page: {}})

        # Get key value
        key = language_value['Key']
        english_value = language_value[language]

        # Assign key to language value
        english_translation_json = {key: english_value}

        if page in language_json:
            language_json[page].update(english_translation_json)

    with open(FLUTTER_PROJECT_PATH + output_json_filename + '.json', 'w') as f:
        json.dump(language_json, f, indent=2)
        return 'Generated ' + language + ' language'
        # print('Generated ' + language + ' language')


def parse_languages(languages_json):
    messages = [parse_language_to_json(languages_json, "English", "en-US"), parse_language_to_json(languages_json, "German", "de-DE"),
                parse_language_to_json(languages_json, "Russian", "ru-RU")]
    return messages


def init_sheets_api_and_get_languages_json():
    scope = ['https://spreadsheets.google.com/feeds', 'https://www.googleapis.com/auth/drive']
    creds = ServiceAccountCredentials.from_json_keyfile_name(CREDENTIALS_JSON_FILE_PATH, scope)
    client = gspread.authorize(creds)
    sheet = client.open(TRANSLATIONS_SHEET_NAME).sheet1
    languages_json_from_sheets = sheet.get_all_records()
    return languages_json_from_sheets


def generate_translations():
    languages_json = init_sheets_api_and_get_languages_json()
    messages = parse_languages(languages_json)
    return messages


def main():
    languages_json = init_sheets_api_and_get_languages_json()
    messages = parse_languages(languages_json)
    print(messages)


main()
